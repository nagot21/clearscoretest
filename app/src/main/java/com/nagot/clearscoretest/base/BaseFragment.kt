package com.nagot.clearscoretest.base

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.support.DaggerFragment

/**
 *   Created by IanNagot on 24/05/2019
 */
abstract class BaseFragment: DaggerFragment() {

    private lateinit var mActivity: AppCompatActivity

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(getLayoutRes(), container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        mActivity = context as AppCompatActivity
    }

    protected abstract fun getLayoutRes(): Int

    fun getBaseActivity() = mActivity
}