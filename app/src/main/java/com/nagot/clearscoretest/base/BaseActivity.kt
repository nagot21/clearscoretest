package com.nagot.clearscoretest.base

import android.os.Bundle
import android.os.PersistableBundle
import dagger.android.support.DaggerAppCompatActivity

/**
 *   Created by IanNagot on 24/05/2019
 */
abstract class BaseActivity: DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {

        super.onCreate(savedInstanceState, persistentState)

        setContentView(getLayoutRes())
    }

    protected abstract fun getLayoutRes(): Int
}