package com.nagot.clearscoretest.util

import android.content.res.Resources

object DensityUtils {

    fun deviceDensity(f: Float): Float {

        return f * Resources.getSystem().displayMetrics.density
    }
}