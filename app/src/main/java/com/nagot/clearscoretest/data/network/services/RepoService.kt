package com.nagot.clearscoretest.data.network.services

import com.nagot.clearscoretest.data.network.dto.MockCredit
import io.reactivex.Single
import retrofit2.http.GET

interface RepoService {

    @GET("prod/mockcredit/values")
    fun getMockCreditValues(): Single<MockCredit>

}