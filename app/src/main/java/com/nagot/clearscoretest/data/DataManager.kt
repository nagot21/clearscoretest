package com.nagot.clearscoretest.data

import com.nagot.clearscoretest.data.network.NetworkManager

/**
 *   Created by IanNagot on 24/05/2019
 */
interface DataManager: NetworkManager {
}