package com.nagot.clearscoretest.data

import com.nagot.clearscoretest.data.network.NetworkManager
import com.nagot.clearscoretest.data.network.dto.MockCredit
import io.reactivex.Single
import javax.inject.Inject

/**
 *   Created by IanNagot on 24/05/2019
 */
class AppDataManager @Inject constructor(private val mNetworkManager: NetworkManager):
    DataManager, NetworkManager {

    override fun getMockCreditsValues(): Single<MockCredit> {

        return mNetworkManager.getMockCreditsValues()
    }
}