package com.nagot.clearscoretest.data.network

import com.nagot.clearscoretest.data.network.dto.MockCredit
import com.nagot.clearscoretest.data.network.services.RepoService
import io.reactivex.Single
import javax.inject.Inject

/**
 *   Created by IanNagot on 24/05/2019
 */
class AppNetworkManager @Inject constructor(private val repoService: RepoService):
    NetworkManager {

    override fun getMockCreditsValues(): Single<MockCredit> {

        return repoService.getMockCreditValues()
    }
}