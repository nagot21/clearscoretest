package com.nagot.clearscoretest.data.network

import com.nagot.clearscoretest.data.network.dto.MockCredit
import io.reactivex.Single

/**
 *   Created by IanNagot on 24/05/2019
 */
interface NetworkManager {

    fun getMockCreditsValues(): Single<MockCredit>
}