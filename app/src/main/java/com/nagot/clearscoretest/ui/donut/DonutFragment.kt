package com.nagot.clearscoretest.ui.donut

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.View
import com.nagot.clearscoretest.R
import com.nagot.clearscoretest.base.BaseFragment
import com.nagot.clearscoretest.factory.ViewModelFactory
import com.robinhood.ticker.TickerUtils
import kotlinx.android.synthetic.main.fragment_donut.*
import javax.inject.Inject

/**
 *   Created by IanNagot on 24/05/2019
 */
class DonutFragment : BaseFragment() {

    @Inject
    lateinit var mViewModelFactory: ViewModelFactory

    lateinit var mViewModel: DonutFragmentViewModel

    companion object {

        const val DONUT_FRAGMENT_TAG = "DonutFragment"

        fun newInstance(): DonutFragment {
            return DonutFragment()
        }
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_donut
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mViewModel = ViewModelProviders.of(
            this, mViewModelFactory
        )
            .get(DonutFragmentViewModel::class.java)

        observableViewModel()
    }

    private fun observableViewModel() {

        mViewModel.getMockCredit().observe(this, Observer { mockCredit ->

                mockCredit?.let {
                    root_view.visibility = View.VISIBLE

                    score_value.apply {
                        setCharacterLists(TickerUtils.provideNumberList())
                        text = mockCredit.creditReportInfo.score.toString()
                    }

                    tv_out_of.text =
                        getString(R.string.donut_score_out_of,
                            mockCredit.creditReportInfo.maxScoreValue)
                }
        })

        mViewModel.getError().observe(this, Observer { error ->

            error?.let {

                if (error) {
                    root_view.visibility = View.GONE
                    error_view.visibility = View.VISIBLE
                } else {
                    root_view.visibility = View.VISIBLE
                    error_view.visibility = View.GONE
                }
            }
        })
    }
}