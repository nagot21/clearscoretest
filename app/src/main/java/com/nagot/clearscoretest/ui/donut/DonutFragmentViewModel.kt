package com.nagot.clearscoretest.ui.donut

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.nagot.clearscoretest.data.DataManager
import com.nagot.clearscoretest.data.network.dto.MockCredit
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DonutFragmentViewModel @Inject constructor(
    private val mAppDataManager: DataManager
) : ViewModel() {

    private var disposable = CompositeDisposable()
    private var mockCredit = MutableLiveData<MockCredit>()
    private var mockCreditError = MutableLiveData<Boolean>()

    init {
        fetchData()
    }

    private fun fetchData() {

        disposable.add(
            mAppDataManager.getMockCreditsValues()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    mockCredit.value = it
                    mockCreditError.value = false
                },
                    {
                        it.printStackTrace()
                        mockCreditError.value = true
                    }))
    }

    fun getMockCredit(): LiveData<MockCredit> = mockCredit

    fun getError(): LiveData<Boolean> = mockCreditError

    override fun onCleared() {
        super.onCleared()

        disposable.clear()
    }
}