package com.nagot.clearscoretest.ui.main

import android.os.Bundle
import com.nagot.clearscoretest.R
import com.nagot.clearscoretest.base.BaseActivity
import com.nagot.clearscoretest.ui.donut.DonutFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    override fun getLayoutRes(): Int {
        return R.layout.activity_main
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(getLayoutRes())

        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.main_container,
                    DonutFragment.newInstance(),
                    DonutFragment.DONUT_FRAGMENT_TAG)
                .commit()
        }

        circle_view.setRadius(100f)
    }
}
