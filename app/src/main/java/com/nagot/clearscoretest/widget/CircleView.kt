package com.nagot.clearscoretest.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Paint.*
import android.util.AttributeSet
import android.view.View
import com.nagot.clearscoretest.R
import com.nagot.clearscoretest.util.DensityUtils

class CircleView @JvmOverloads
constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) :
    View(context, attrs, defStyle) {

    private var circleColor: Int = -1
    private var circleThickness: Float = DensityUtils.deviceDensity(10f)
    private var circleRadius: Float = 0f
    private var paint: Paint

    init {

        attrs?.let {

            val attributes = context.obtainStyledAttributes(
                attrs,
                R.styleable.CircleView, 0, 0
            )

            circleThickness =
                attributes
                    .getFloat(
                        R.styleable.CircleView_circleThickness,
                        circleThickness
                    )

            circleColor =
                attributes
                    .getColor(
                        R.styleable.CircleView_circleColor,
                        circleColor
                    )

            attributes.recycle()
        }

        paint = Paint(1).apply {

            style = Style.STROKE
            color = circleColor
            strokeWidth = circleThickness

        }


    }

    fun setRadius(radius: Float) {

        circleRadius = radius

        invalidate()
    }

    fun setColor(color: Int) {

        circleColor = color

        invalidate()
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        canvas?.let {
            canvas.drawCircle(
                measuredWidth.toFloat() / 2f,
                measuredHeight.toFloat() / 2f,
                circleRadius, paint
            )
        }
    }
}