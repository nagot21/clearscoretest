package com.nagot.clearscoretest.di.module

import android.arch.lifecycle.ViewModel
import com.nagot.clearscoretest.di.scope.ViewModelKey
import com.nagot.clearscoretest.ui.donut.DonutFragmentViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
internal abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(DonutFragmentViewModel::class)
    internal abstract fun bindDonutFragmentViewModel(
        donutFragmentViewModel:
        DonutFragmentViewModel
    ): ViewModel
}