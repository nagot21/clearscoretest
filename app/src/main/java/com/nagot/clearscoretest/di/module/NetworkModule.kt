package com.nagot.clearscoretest.di.module

import android.support.annotation.NonNull
import com.nagot.clearscoretest.data.network.services.RepoService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 *   Created by IanNagot on 24/05/2019
 */

@Module
class NetworkModule {

    companion object {
        private const val BASE_URL = "https://5lfoiyb0b3.execute-api.us-west-2.amazonaws.com/"
    }

    @Provides
    @Singleton
    fun provideInterceptor(): HttpLoggingInterceptor {

        return HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    }

    @Provides
    @Singleton
    fun provideHttpClient(@NonNull interceptor: HttpLoggingInterceptor):
            OkHttpClient {

        return OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(@NonNull okHttpClient: OkHttpClient): Retrofit {

        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    @Provides
    @Singleton
    fun provideCreditService(@NonNull retrofit: Retrofit): RepoService {

        return retrofit.create(RepoService::class.java)
    }
}