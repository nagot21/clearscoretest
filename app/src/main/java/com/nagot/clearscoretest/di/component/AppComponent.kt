package com.nagot.clearscoretest.di.component

import android.app.Application
import com.nagot.clearscoretest.BaseApplication
import com.nagot.clearscoretest.di.module.ActivityModule
import com.nagot.clearscoretest.di.module.ApplicationModule
import com.nagot.clearscoretest.di.module.NetworkModule
import com.nagot.clearscoretest.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import dagger.android.support.DaggerApplication
import javax.inject.Singleton

/**
 *   Created by IanNagot on 24/05/2019
 */

@Singleton
@Component(
    modules = [AndroidSupportInjectionModule::class,
        ActivityModule::class,
        ApplicationModule::class,
        NetworkModule::class,
        ViewModelModule::class]
)

interface AppComponent : AndroidInjector<DaggerApplication> {

    fun inject(baseApplication: BaseApplication)

    @Component.Builder
    interface Builder {
        @BindsInstance

        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}