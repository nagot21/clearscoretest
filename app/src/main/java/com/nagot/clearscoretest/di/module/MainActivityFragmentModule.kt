package com.nagot.clearscoretest.di.module

import com.nagot.clearscoretest.ui.donut.DonutFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 *   Created by IanNagot on 24/05/2019
 */

@Module
abstract class MainActivityFragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeDonutFragment(): DonutFragment
}