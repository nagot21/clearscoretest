package com.nagot.clearscoretest.di.module

import com.nagot.clearscoretest.ui.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 *   Created by IanNagot on 24/05/2019
 */

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector(modules = [MainActivityFragmentModule::class])
    abstract fun contributeMainActivity(): MainActivity
}