package com.nagot.clearscoretest.di.module

import android.support.annotation.NonNull
import com.nagot.clearscoretest.data.AppDataManager
import com.nagot.clearscoretest.data.DataManager
import com.nagot.clearscoretest.data.network.AppNetworkManager
import com.nagot.clearscoretest.data.network.NetworkManager
import com.nagot.clearscoretest.data.network.services.RepoService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 *   Created by IanNagot on 24/05/2019
 */

@Module
class ApplicationModule {

    @Provides
    @Singleton
    fun provideNetworkManager(@NonNull repoService: RepoService): NetworkManager {

        return AppNetworkManager(repoService)
    }

    @Provides
    @Singleton
    fun provideDataHelper(@NonNull networkManager: NetworkManager): DataManager {

        return AppDataManager(networkManager)
    }
}