package com.nagot.clearscoretest

import com.nagot.clearscoretest.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

/**
 *   Created by IanNagot on 24/05/2019
 */
class BaseApplication: DaggerApplication() {

    override fun onCreate() {
        super.onCreate()
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {

        val component = DaggerAppComponent.builder().application(this).build()

        component.inject(this)

        return component
    }
}